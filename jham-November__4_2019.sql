-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2019 at 12:22 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jham`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicant_personal`
--

CREATE TABLE IF NOT EXISTS `applicant_personal` (
  `personal_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `mname` varchar(255) NOT NULL,
  `suffix` varchar(255) NOT NULL,
  `dbirth` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `pbirth` varchar(255) NOT NULL,
  `civil` varchar(255) NOT NULL,
  `citizenship` varchar(255) NOT NULL,
  `height` varchar(255) NOT NULL,
  `weight` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mphone1` varchar(15) NOT NULL,
  `mphone2` varchar(15) NOT NULL,
  `street` varchar(255) NOT NULL,
  `barangay` varchar(255) NOT NULL,
  `municipality` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `street_p` varchar(255) NOT NULL,
  `barangay_p` varchar(255) NOT NULL,
  `municipality_p` varchar(255) NOT NULL,
  `province_p` varchar(255) NOT NULL,
  `disability` varchar(255) NOT NULL,
  `other_disability` varchar(255) NOT NULL,
  `employment` varchar(255) NOT NULL,
  `emp_status` varchar(255) NOT NULL,
  `looking_work` varchar(255) NOT NULL,
  `looking_work_status` varchar(255) NOT NULL,
  `willing_work` varchar(255) NOT NULL,
  `when_work` varchar(255) NOT NULL,
  `four_ps` varchar(255) NOT NULL,
  `four_ps_number` varchar(255) NOT NULL,
  `ofw` varchar(255) NOT NULL,
  `work_back` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_personal`
--

INSERT INTO `applicant_personal` (`personal_id`, `applicant_id`, `fname`, `lname`, `mname`, `suffix`, `dbirth`, `age`, `sex`, `pbirth`, `civil`, `citizenship`, `height`, `weight`, `phone`, `mphone1`, `mphone2`, `street`, `barangay`, `municipality`, `province`, `street_p`, `barangay_p`, `municipality_p`, `province_p`, `disability`, `other_disability`, `employment`, `emp_status`, `looking_work`, `looking_work_status`, `willing_work`, `when_work`, `four_ps`, `four_ps_number`, `ofw`, `work_back`, `date`) VALUES
(1, 4, 'First', 'Last', 'Middle', '', '10-10-1999', 20, 'male', 'birth', 'Single', '', '', '', '', '+639774243247', '', 'rizal', '17', 'Legazpi', 'Albay', 'rizal', '17', 'Legazpi', 'Albay', '0', ' ', 'Employed', 'Wage Employed', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', ' ', '2019-11-03'),
(3, 6, 'f n', 'l n', 'm n', '', '05-03-2000', 19, 'male', 'pob', 'Single', '', '', '', '', '+639774243247', '', 'rizal', '17', 'Legazpi', 'Albay', 'rizal', '17', 'Legazpi', 'Albay', '0', ' ', 'Employed', 'Wage Employed', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', ' ', '2019-11-03'),
(4, 7, 'Harly', 'Napay', 'Mujar', '', '06-27-1983', 36, 'male', 'Daraga', 'Married', 'Filipino', '163', '70', '', '+639774243247', '', '380 Sto Cristo Street', 'San Roque', 'Daraga', 'Albay', '380 Sto Cristo Street', 'San Roque', 'Daraga', 'Albay', '0', ' ', 'Upemployed', 'Resigned', 'Yes', '', 'Yes', '11-19', 'No', '', 'No', ' ', '2019-11-03'),
(5, 9, 'om', 'mo', 'a', '', '07-22-1997', 22, 'male', 'Daraga, Albay', 'Single', '', '', '', '', '+639186455311', '', 'rizal', 'Bgy. 10 - Cabugao', 'Legazpi', 'Albay', 'rizal', 'Bgy. 20 - Cabagñan East (Pob.)', 'Legazpi', 'Albay', '0', ' ', 'Employed', 'Wage Employed', 'Yes', '', 'Yes', '', 'Yes', '', 'Yes', ' ', '2019-11-03');

-- --------------------------------------------------------

--
-- Table structure for table `applied_job`
--

CREATE TABLE IF NOT EXISTS `applied_job` (
  `app_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `app_job_id` int(11) NOT NULL,
  `app_date` varchar(255) NOT NULL,
  `app_status` int(11) NOT NULL,
  `date_hired` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied_job`
--

INSERT INTO `applied_job` (`app_id`, `app_user_id`, `app_job_id`, `app_date`, `app_status`, `date_hired`) VALUES
(1, 19, 14, '31-10-19', 3, ''),
(2, 6, 21, '03-11-19', 3, ''),
(3, 7, 27, '03-11-19', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `app_picture`
--

CREATE TABLE IF NOT EXISTS `app_picture` (
  `pic_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `app_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `barangays`
--

CREATE TABLE IF NOT EXISTS `barangays` (
  `barangay_id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `city_municipality_id` int(11) NOT NULL,
  `barangay_name` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=447 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `barangays`
--

INSERT INTO `barangays` (`barangay_id`, `province_id`, `city_municipality_id`, `barangay_name`) VALUES
(1, 1, 1, 'Bgy. 47 - Arimbay'),
(2, 1, 1, 'Bgy. 64 - Bagacay (Bgy. 41 Bagacay)'),
(3, 1, 1, 'Bgy. 48 - Bagong Abre (Bgy. 42)'),
(4, 1, 1, 'Bgy. 66 - Banquerohan (Bgy. 43)'),
(5, 1, 1, 'Bgy. 1 - Em''s Barrio (Pob.)'),
(6, 1, 1, 'Bgy. 11 - Maoyod Pob. (Bgy. 10 & 11)'),
(7, 1, 1, 'Bgy. 12 - Tula-tula (Pob.)'),
(8, 1, 1, 'Bgy. 13 - Ilawod West Pob. (Ilawod 1)'),
(9, 1, 1, 'Bgy. 14 - Ilawod Pob. (Ilawod 2)'),
(10, 1, 1, 'Bgy. 15 - Ilawod East Pob. (Ilawod 3)'),
(11, 1, 1, 'Bgy. 16 - Kawit-East Washington Drive (Pob.)'),
(12, 1, 1, 'Bgy. 17 - Rizal Sreet., Ilawod (Pob.)'),
(13, 1, 1, 'Bgy. 19 - Cabagñan'),
(14, 1, 1, 'Bgy. 2 - Em''s Barrio South (Pob.)'),
(15, 1, 1, 'Bgy. 18 - Cabagñan West (Pob.)'),
(16, 1, 1, 'Bgy. 21 - Binanuahan West (Pob.)'),
(17, 1, 1, 'Bgy. 22 - Binanuahan East (Pob.)'),
(18, 1, 1, 'Bgy. 23 - Imperial Court Subd. (Pob.)'),
(19, 1, 1, 'Bgy. 20 - Cabagñan East (Pob.)'),
(20, 1, 1, 'Bgy. 25 - Lapu-lapu (Pob.)'),
(21, 1, 1, 'Bgy. 26 - Dinagaan (Pob.)'),
(22, 1, 1, 'Bgy. 27 - Victory Village South (Pob.)'),
(23, 1, 1, 'Bgy. 28 - Victory Village North (Pob.)'),
(24, 1, 1, 'Bgy. 29 - Sabang (Pob.)'),
(25, 1, 1, 'Bgy. 3 - Em''s Barrio East (Pob.)'),
(26, 1, 1, 'Bgy. 36 - Kapantawan (Pob.)'),
(27, 1, 1, 'Bgy. 30 - Pigcale (Pob.)'),
(28, 1, 1, 'Bgy. 31 - Centro-Baybay (Pob.)'),
(29, 1, 1, 'Bgy. 33 - PNR-Peñaranda St.-Iraya (Pob.)'),
(30, 1, 1, 'Bgy. 34 - Oro Site-Magallanes St. (Pob.)'),
(31, 1, 1, 'Bgy. 35 - Tinago (Pob.)'),
(32, 1, 1, 'Bgy. 37 - Bitano (Pob.)'),
(33, 1, 1, 'Bgy. 39 - Bonot (Pob.)'),
(34, 1, 1, 'Bgy. 4 - Sagpon Pob. (Sagpon 1)'),
(35, 1, 1, 'Bgy. 5 - Sagmin Pob. (Sagpon 2)'),
(36, 1, 1, 'Bgy. 6 - Bañadero Pob. (Sagpon 3)'),
(37, 1, 1, 'Bgy. 7 - Baño (Pob.)'),
(38, 1, 1, 'Bgy. 8 - Bagumbayan (Pob.)'),
(39, 1, 1, 'Bgy. 9 - Pinaric (Pob.)'),
(40, 1, 1, 'Bgy. 67 - Bariis (Bgy. 46)'),
(41, 1, 1, 'Bgy. 49 - Bigaa (Bgy. 44)'),
(42, 1, 1, 'Bgy. 41 - Bogtong (Bgy. 45)'),
(43, 1, 1, 'Bgy. 53 - Bonga (Bgy. 48)'),
(44, 1, 1, 'Bgy. 69 - Buenavista (Bgy.47)'),
(45, 1, 1, 'Bgy. 51 - Buyuan (Bgy. 49)'),
(46, 1, 1, 'Bgy. 70 - Cagbacong (Bgy. 50)'),
(47, 1, 1, 'Bgy. 40 - Cruzada (Bgy. 52)'),
(48, 1, 1, 'Bgy. 57 - Dap-dap (Bgy. 69)'),
(49, 1, 1, 'Bgy. 45 - Dita (Bgy. 51)'),
(50, 1, 1, 'Bgy. 55 - Estanza (Bgy. 53)'),
(51, 1, 1, 'Bgy. 38 - Gogon (Bgy. 54)'),
(52, 1, 1, 'Bgy. 62 - Homapon (Bgy. 55)'),
(53, 1, 1, 'Bgy. 65 - Imalnod (Bgy. 57)'),
(54, 1, 1, 'Bgy. 54 - Mabinit (Bgy. 59)'),
(55, 1, 1, 'Bgy. 63 - Mariawa (Bgy. 56)'),
(56, 1, 1, 'Bgy. 61 - Maslog (Bgy. 58)'),
(57, 1, 1, 'Bgy. 50 - Padang (Bgy. 60)'),
(58, 1, 1, 'Bgy. 44 - Pawa (Bgy. 61)'),
(59, 1, 1, 'Bgy. 59 - Puro (Bgy. 63)'),
(60, 1, 1, 'Bgy. 42 - Rawis (Bgy. 65)'),
(61, 1, 1, 'Bgy. 68 - San Francisco (Bgy. 62)'),
(62, 1, 1, 'Bgy. 46 - San Joaquin (Bgy. 64)'),
(63, 1, 1, 'Bgy. 32 - San Roque (Bgy. 66)'),
(64, 1, 1, 'Bgy. 43 - Tamaoyan (Bgy. 67)'),
(65, 1, 1, 'Bgy. 56 - Taysan (Bgy. 68)'),
(66, 1, 1, 'Bgy. 52 - Matanag'),
(67, 1, 1, 'Bgy. 10 - Cabugao'),
(68, 1, 1, 'Bgy. 24 - Rizal Street'),
(69, 1, 1, 'Bgy. 58 - Buragwis'),
(70, 1, 1, 'Bgy. 60 - Lamba'),
(71, 1, 2, 'Abella'),
(72, 1, 2, 'Allang'),
(73, 1, 2, 'Amtic'),
(74, 1, 2, 'Bacong'),
(75, 1, 2, 'Bagumbayan'),
(76, 1, 2, 'Balanac'),
(77, 1, 2, 'Baligang'),
(78, 1, 2, 'Barayong'),
(79, 1, 2, 'Basag'),
(80, 1, 2, 'Batang'),
(81, 1, 2, 'Bay'),
(82, 1, 2, 'Binanowan'),
(83, 1, 2, 'Binatagan (Pob.)'),
(84, 1, 2, 'Bobonsuran'),
(85, 1, 2, 'Bonga'),
(86, 1, 2, 'Busac'),
(87, 1, 2, 'Busay'),
(88, 1, 2, 'Cabarian'),
(89, 1, 2, 'Calzada (Pob.)'),
(90, 1, 2, 'Catburawan'),
(91, 1, 2, 'Cavasi'),
(92, 1, 2, 'Culliat'),
(93, 1, 2, 'Dunao'),
(94, 1, 2, 'Francia'),
(95, 1, 2, 'Guilid'),
(96, 1, 2, 'Herrera'),
(97, 1, 2, 'Layon'),
(98, 1, 2, 'Macalidong'),
(99, 1, 2, 'Mahaba'),
(100, 1, 2, 'Malama'),
(101, 1, 2, 'Maonon'),
(102, 1, 2, 'Nasisi'),
(103, 1, 2, 'Nabonton'),
(104, 1, 2, 'Oma-oma'),
(105, 1, 2, 'Palapas'),
(106, 1, 2, 'Pandan'),
(107, 1, 2, 'Paulba'),
(108, 1, 2, 'Paulog'),
(109, 1, 2, 'Pinamaniquian'),
(110, 1, 2, 'Pinit'),
(111, 1, 2, 'Ranao-ranao'),
(112, 1, 2, 'San Vicente'),
(113, 1, 2, 'Santa Cruz (Pob.)'),
(114, 1, 2, 'Tagpo'),
(115, 1, 2, 'Tambo'),
(116, 1, 2, 'Tandarora'),
(117, 1, 2, 'Tastas'),
(118, 1, 2, 'Tinago'),
(119, 1, 2, 'Tinampo'),
(120, 1, 2, 'Tiongson'),
(121, 1, 2, 'Tomolin'),
(122, 1, 2, 'Tuburan'),
(123, 1, 2, 'Tula-tula Grande'),
(124, 1, 2, 'Tula-tula Pequeño'),
(125, 1, 2, 'Tupas'),
(126, 1, 3, 'Agnas (San Miguel Island)'),
(127, 1, 3, 'Bacolod'),
(128, 1, 3, 'Bangkilingan'),
(129, 1, 3, 'Bantayan'),
(130, 1, 3, 'Baranghawon'),
(131, 1, 3, 'Basagan'),
(132, 1, 3, 'Basud (Pob.)'),
(133, 1, 3, 'Bogñabong'),
(134, 1, 3, 'Bombon (Pob.)'),
(135, 1, 3, 'Bonot'),
(136, 1, 3, 'San Isidro (Boring)'),
(137, 1, 3, 'Buang'),
(138, 1, 3, 'Buhian'),
(139, 1, 3, 'Cabagñan'),
(140, 1, 3, 'Cobo'),
(141, 1, 3, 'Comon'),
(142, 1, 3, 'Cormidal'),
(143, 1, 3, 'Divino Rostro (Pob.)'),
(144, 1, 3, 'Fatima'),
(145, 1, 3, 'Guinobat'),
(146, 1, 3, 'Hacienda (San Miguel Island)'),
(147, 1, 3, 'Magapo'),
(148, 1, 3, 'Mariroc'),
(149, 1, 3, 'Matagbac'),
(150, 1, 3, 'Oras'),
(151, 1, 3, 'Oson'),
(152, 1, 3, 'Panal'),
(153, 1, 3, 'Pawa'),
(154, 1, 3, 'Pinagbobong'),
(155, 1, 3, 'Quinale Cabasan (Pob.)'),
(156, 1, 3, 'Quinastillojan'),
(157, 1, 3, 'Rawis (San Miguel Island)'),
(158, 1, 3, 'Sagurong (San Miguel Island)'),
(159, 1, 3, 'Salvacion'),
(160, 1, 3, 'San Antonio'),
(161, 1, 3, 'San Carlos'),
(162, 1, 3, 'San Juan (Pob.)'),
(163, 1, 3, 'San Lorenzo'),
(164, 1, 3, 'San Ramon'),
(165, 1, 3, 'San Roque'),
(166, 1, 3, 'San Vicente'),
(167, 1, 3, 'Santo Cristo (Pob.)'),
(168, 1, 3, 'Sua-Igot'),
(169, 1, 3, 'Tabiguian'),
(170, 1, 3, 'Tagas'),
(171, 1, 3, 'Tayhi (Pob.)'),
(172, 1, 3, 'Visita (San Miguel Island)'),
(173, 1, 4, 'Baclayon'),
(174, 1, 4, 'Banao'),
(175, 1, 4, 'Bariw'),
(176, 1, 4, 'Basud'),
(177, 1, 4, 'Bayandong'),
(178, 1, 4, 'Bonga (Upper)'),
(179, 1, 4, 'Buang'),
(180, 1, 4, 'Cabasan'),
(181, 1, 4, 'Cagbulacao'),
(182, 1, 4, 'Cagraray'),
(183, 1, 4, 'Cajogutan'),
(184, 1, 4, 'Cawayan'),
(185, 1, 4, 'Damacan'),
(186, 1, 4, 'Gubat Ilawod'),
(187, 1, 4, 'Gubat Iraya'),
(188, 1, 4, 'Hindi'),
(189, 1, 4, 'Igang'),
(190, 1, 4, 'Langaton'),
(191, 1, 4, 'Manaet'),
(192, 1, 4, 'Mapulang Daga'),
(193, 1, 4, 'Mataas'),
(194, 1, 4, 'Misibis'),
(195, 1, 4, 'Nahapunan'),
(196, 1, 4, 'Namanday'),
(197, 1, 4, 'Namantao'),
(198, 1, 4, 'Napao'),
(199, 1, 4, 'Panarayon'),
(200, 1, 4, 'Pigcobohan'),
(201, 1, 4, 'Pili Ilawod'),
(202, 1, 4, 'Pili Iraya'),
(203, 1, 4, 'Barangay 1 (Pob.)'),
(204, 1, 4, 'Barangay 10 (Pob.)'),
(205, 1, 4, 'Barangay 11 (Pob.)'),
(206, 1, 4, 'Barangay 12 (Pob.)'),
(207, 1, 4, 'Barangay 13 (Pob.)'),
(208, 1, 4, 'Barangay 14 (Pob.)'),
(209, 1, 4, 'Barangay 2 (Pob.)'),
(210, 1, 4, 'Barangay 3 (Pob.)'),
(211, 1, 4, 'Barangay 4 (Pob.)'),
(212, 1, 4, 'Barangay 5 (Pob.)'),
(213, 1, 4, 'Barangay 6 (Pob.)'),
(214, 1, 4, 'Barangay 7 (Pob.)'),
(215, 1, 4, 'Barangay 8 (Pob.)'),
(216, 1, 4, 'Barangay 9 (Pob.)'),
(217, 1, 4, 'Pongco (Lower Bonga)'),
(218, 1, 4, 'Busdac (San Jose)'),
(219, 1, 4, 'San Pablo'),
(220, 1, 4, 'San Pedro'),
(221, 1, 4, 'Sogod'),
(222, 1, 4, 'Sula'),
(223, 1, 4, 'Tambilagao (Tambognon)'),
(224, 1, 4, 'Tambongon (Tambilagao)'),
(225, 1, 4, 'Tanagan'),
(226, 1, 4, 'Uson'),
(227, 1, 4, 'Vinisitahan-Basud (Mainland)'),
(228, 1, 4, 'Vinisitahan-Napao (lsland)'),
(229, 1, 5, 'Anoling'),
(230, 1, 5, 'Baligang'),
(231, 1, 5, 'Bantonan'),
(232, 1, 5, 'Bariw'),
(233, 1, 5, 'Binanderahan'),
(234, 1, 5, 'Binitayan'),
(235, 1, 5, 'Bongabong'),
(236, 1, 5, 'Cabagñan'),
(237, 1, 5, 'Cabraran Pequeño'),
(238, 1, 5, 'Calabidongan'),
(239, 1, 5, 'Comun'),
(240, 1, 5, 'Cotmon'),
(241, 1, 5, 'Del Rosario'),
(242, 1, 5, 'Gapo'),
(243, 1, 5, 'Gotob'),
(244, 1, 5, 'Ilawod'),
(245, 1, 5, 'Iluluan'),
(246, 1, 5, 'Libod'),
(247, 1, 5, 'Ligban'),
(248, 1, 5, 'Mabunga'),
(249, 1, 5, 'Magogon'),
(250, 1, 5, 'Manawan'),
(251, 1, 5, 'Maninila'),
(252, 1, 5, 'Mina'),
(253, 1, 5, 'Miti'),
(254, 1, 5, 'Palanog'),
(255, 1, 5, 'Panoypoy'),
(256, 1, 5, 'Pariaan'),
(257, 1, 5, 'Quinartilan'),
(258, 1, 5, 'Quirangay'),
(259, 1, 5, 'Quitinday'),
(260, 1, 5, 'Salugan'),
(261, 1, 5, 'Solong'),
(262, 1, 5, 'Sua'),
(263, 1, 5, 'Sumlang'),
(264, 1, 5, 'Tagaytay'),
(265, 1, 5, 'Tagoytoy'),
(266, 1, 5, 'Taladong'),
(267, 1, 5, 'Taloto'),
(268, 1, 5, 'Taplacon'),
(269, 1, 5, 'Tinago'),
(270, 1, 5, 'Tumpa'),
(271, 1, 5, 'Barangay 1 (Pob.)'),
(272, 1, 5, 'Barangay 2 (Pob.)'),
(273, 1, 5, 'Barangay 3 (Pob.)'),
(274, 1, 5, 'Barangay 4 (Pob.)'),
(275, 1, 5, 'Barangay 5 (Pob.)'),
(276, 1, 5, 'Barangay 6 (Pob.)'),
(277, 1, 5, 'Barangay 7 (Pob.)'),
(278, 1, 5, 'Caguiba'),
(279, 1, 6, 'Alcala'),
(280, 1, 6, 'Alobo'),
(281, 1, 6, 'Anislag'),
(282, 1, 6, 'Bagumbayan'),
(283, 1, 6, 'Balinad'),
(284, 1, 6, 'Bañadero'),
(285, 1, 6, 'Bañag'),
(286, 1, 6, 'Bascaran'),
(287, 1, 6, 'Bigao'),
(288, 1, 6, 'Binitayan'),
(289, 1, 6, 'Bongalon'),
(290, 1, 6, 'Budiao'),
(291, 1, 6, 'Burgos'),
(292, 1, 6, 'Busay'),
(293, 1, 6, 'Cullat'),
(294, 1, 6, 'Canaron'),
(295, 1, 6, 'Dela Paz'),
(296, 1, 6, 'Dinoronan'),
(297, 1, 6, 'Gabawan'),
(298, 1, 6, 'Gapo'),
(299, 1, 6, 'Ibaugan'),
(300, 1, 6, 'Ilawod Area Pob. (Dist. 2)'),
(301, 1, 6, 'Inarado'),
(302, 1, 6, 'Kidaco'),
(303, 1, 6, 'Kilicao'),
(304, 1, 6, 'Kimantong'),
(305, 1, 6, 'Kinawitan'),
(306, 1, 6, 'Kiwalo'),
(307, 1, 6, 'Lacag'),
(308, 1, 6, 'Mabini'),
(309, 1, 6, 'Malabog'),
(310, 1, 6, 'Malobago'),
(311, 1, 6, 'Maopi'),
(312, 1, 6, 'Market Area Pob. (Dist. 1)'),
(313, 1, 6, 'Maroroy'),
(314, 1, 6, 'Matnog'),
(315, 1, 6, 'Mayon'),
(316, 1, 6, 'Mi-isi'),
(317, 1, 6, 'Nabasan'),
(318, 1, 6, 'Namantao'),
(319, 1, 6, 'Pandan'),
(320, 1, 6, 'Peñafrancia'),
(321, 1, 6, 'Sagpon'),
(322, 1, 6, 'Salvacion'),
(323, 1, 6, 'San Rafael'),
(324, 1, 6, 'San Ramon'),
(325, 1, 6, 'San Roque'),
(326, 1, 6, 'San Vicente Grande'),
(327, 1, 6, 'San Vicente Pequeño'),
(328, 1, 6, 'Sipi'),
(329, 1, 6, 'Tabon-tabon'),
(330, 1, 6, 'Tagas'),
(331, 1, 6, 'Talahib'),
(332, 1, 6, 'Villahermosa'),
(333, 1, 7, 'Agpay'),
(334, 1, 7, 'Balite'),
(335, 1, 7, 'Banao'),
(336, 1, 7, 'Batbat'),
(337, 1, 7, 'Binogsacan Lower'),
(338, 1, 7, 'Bololo'),
(339, 1, 7, 'Bubulusan'),
(340, 1, 7, 'Marcial O. Rañola (Cabaloaon)'),
(341, 1, 7, 'Calzada'),
(342, 1, 7, 'Catomag'),
(343, 1, 7, 'Doña Mercedes'),
(344, 1, 7, 'Doña Tomasa (Magatol)'),
(345, 1, 7, 'Ilawod'),
(346, 1, 7, 'Inamnan Pequeño'),
(347, 1, 7, 'Inamnan Grande'),
(348, 1, 7, 'Inascan'),
(349, 1, 7, 'Iraya'),
(350, 1, 7, 'Lomacao'),
(351, 1, 7, 'Maguiron'),
(352, 1, 7, 'Maipon'),
(353, 1, 7, 'Malabnig'),
(354, 1, 7, 'Malipo'),
(355, 1, 7, 'Malobago'),
(356, 1, 7, 'Maninila'),
(357, 1, 7, 'Mapaco'),
(358, 1, 7, 'Masarawag'),
(359, 1, 7, 'Mauraro'),
(360, 1, 7, 'Minto'),
(361, 1, 7, 'Morera'),
(362, 1, 7, 'Muladbucad Pequeño'),
(363, 1, 7, 'Muladbucad Grande'),
(364, 1, 7, 'Ongo'),
(365, 1, 7, 'Palanas'),
(366, 1, 7, 'Poblacion'),
(367, 1, 7, 'Pood'),
(368, 1, 7, 'Quitago'),
(369, 1, 7, 'Quibongbongan'),
(370, 1, 7, 'San Francisco'),
(371, 1, 7, 'San Jose (Ogsong)'),
(372, 1, 7, 'San Rafael'),
(373, 1, 7, 'Sinungtan'),
(374, 1, 7, 'Tandarora'),
(375, 1, 7, 'Travesia'),
(376, 1, 7, 'Binogsacan Upper'),
(377, 1, 8, 'Bagacay'),
(378, 1, 8, 'Rizal Pob. (Bgy. 1)'),
(379, 1, 8, 'Mabini Pob. (Bgy. 2)'),
(380, 1, 8, 'Plaza Pob. (Bgy. 3)'),
(381, 1, 8, 'Magsaysay Pob (Bgy. 4)'),
(382, 1, 8, 'Calzada Pob. (Bgy. 7)'),
(383, 1, 8, 'Quitinday Pob. (Bgy. 8)'),
(384, 1, 8, 'White Deer Pob. (Bgy. 9)'),
(385, 1, 8, 'Bautista'),
(386, 1, 8, 'Cabraran'),
(387, 1, 8, 'Del Rosario'),
(388, 1, 8, 'Estrella'),
(389, 1, 8, 'Florista'),
(390, 1, 8, 'Mamlad'),
(391, 1, 8, 'Maogog'),
(392, 1, 8, 'Mercado Pob. (Bgy. 5)'),
(393, 1, 8, 'Salvacion'),
(394, 1, 8, 'San Isidro'),
(395, 1, 8, 'San Roque'),
(396, 1, 8, 'San Vicente'),
(397, 1, 8, 'Sinagaran'),
(398, 1, 8, 'Villa Paz'),
(399, 1, 8, 'Aurora Pob. (Bgy. 6)'),
(400, 1, 9, 'Alongong'),
(401, 1, 9, 'Apud'),
(402, 1, 9, 'Bacolod'),
(403, 1, 9, 'Zone I (Pob.)'),
(404, 1, 9, 'Zone II (Pob.)'),
(405, 1, 9, 'Zone III (Pob.)'),
(406, 1, 9, 'Zone IV (Pob.)'),
(407, 1, 9, 'Zone V (Pob.)'),
(408, 1, 9, 'Zone VI (Pob.)'),
(409, 1, 9, 'Zone VII (Pob.)'),
(410, 1, 9, 'Bariw'),
(411, 1, 9, 'Bonbon'),
(412, 1, 9, 'Buga'),
(413, 1, 9, 'Bulusan'),
(414, 1, 9, 'Burabod'),
(415, 1, 9, 'Caguscos'),
(416, 1, 9, 'East Carisac'),
(417, 1, 9, 'West Carisac'),
(418, 1, 9, 'Harigue'),
(419, 1, 9, 'Libtong'),
(420, 1, 9, 'Linao'),
(421, 1, 9, 'Mabayawas'),
(422, 1, 9, 'Macabugos'),
(423, 1, 9, 'Magallang'),
(424, 1, 9, 'Malabiga'),
(425, 1, 9, 'Marayag'),
(426, 1, 9, 'Matara'),
(427, 1, 9, 'Molosbolos'),
(428, 1, 9, 'Natasan'),
(429, 1, 9, 'Nogpo'),
(430, 1, 9, 'Pantao'),
(431, 1, 9, 'Rawis'),
(432, 1, 9, 'Sagrada Familia'),
(433, 1, 9, 'Salvacion'),
(434, 1, 9, 'Sampongan'),
(435, 1, 9, 'San Agustin'),
(436, 1, 9, 'San Antonio'),
(437, 1, 9, 'San Isidro'),
(438, 1, 9, 'San Jose'),
(439, 1, 9, 'San Pascual'),
(440, 1, 9, 'San Ramon'),
(441, 1, 9, 'San Vicente'),
(442, 1, 9, 'Santa Cruz'),
(443, 1, 9, 'Niño Jesus (Santo Niño Jesus)'),
(444, 1, 9, 'Talin-talin'),
(445, 1, 9, 'Tambo'),
(446, 1, 9, 'Villa Petrona');

-- --------------------------------------------------------

--
-- Table structure for table `century_skill`
--

CREATE TABLE IF NOT EXISTS `century_skill` (
  `cent_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `inovation` int(11) NOT NULL,
  `team_work` int(11) NOT NULL,
  `multitasking` int(11) NOT NULL,
  `work_ethics` int(11) NOT NULL,
  `self_motivation` int(11) NOT NULL,
  `creative_problem` int(11) NOT NULL,
  `problem_solving` int(11) NOT NULL,
  `critical_thinking` int(11) NOT NULL,
  `decision_making` int(11) NOT NULL,
  `strees_tolerance` int(11) NOT NULL,
  `planing` int(11) NOT NULL,
  `perceptiveness` int(11) NOT NULL,
  `english_funtional` int(11) NOT NULL,
  `english_comp` int(11) NOT NULL,
  `math_functional` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `century_skill`
--

INSERT INTO `century_skill` (`cent_id`, `app_user_id`, `inovation`, `team_work`, `multitasking`, `work_ethics`, `self_motivation`, `creative_problem`, `problem_solving`, `critical_thinking`, `decision_making`, `strees_tolerance`, `planing`, `perceptiveness`, `english_funtional`, `english_comp`, `math_functional`) VALUES
(1, 4, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0),
(2, 5, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(3, 6, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 8, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(5, 7, 0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0),
(6, 9, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `city_municipality`
--

CREATE TABLE IF NOT EXISTS `city_municipality` (
  `city_municipality_id` int(11) NOT NULL DEFAULT '0',
  `province_id` int(11) DEFAULT NULL,
  `city_municipality_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city_municipality`
--

INSERT INTO `city_municipality` (`city_municipality_id`, `province_id`, `city_municipality_name`) VALUES
(1, 1, 'Legazpi'),
(2, 1, 'Ligao'),
(3, 1, 'Tabaco'),
(4, 1, 'Bacacay'),
(5, 1, 'Camalig'),
(6, 1, 'Daraga'),
(7, 1, 'Guinobatan'),
(8, 1, 'Jovellar'),
(9, 1, 'Libon'),
(10, 1, 'Malilipot'),
(11, 1, 'Malinao'),
(12, 1, 'Manito'),
(13, 1, 'Oas'),
(14, 1, 'Pio Duran'),
(15, 1, 'Polangui'),
(16, 1, 'Rapu-Rapu'),
(17, 1, 'Santo Domingo'),
(18, 1, 'Tiwi'),
(19, 2, 'Basud'),
(20, 2, 'Capalonga'),
(21, 2, 'Daet '),
(22, 2, 'Jose Panganiban'),
(23, 2, 'Labo'),
(24, 2, 'Mercedes'),
(25, 2, 'Paracale'),
(26, 2, 'San Lorenzo Ruiz'),
(27, 2, 'San Vicente'),
(28, 2, 'Santa Elena'),
(29, 2, 'Talisay'),
(30, 2, 'Vinzons'),
(31, 3, 'Naga City '),
(32, 3, 'Iriga City '),
(33, 3, 'Baao'),
(34, 3, 'Balatan'),
(35, 3, 'Bato'),
(36, 3, 'Bombon'),
(37, 3, 'Buhi'),
(38, 3, 'Bula'),
(39, 3, 'Cabusao'),
(40, 3, 'Calabanga'),
(41, 3, 'Camaligan'),
(42, 3, 'Canaman'),
(43, 3, 'Caramoan'),
(44, 3, 'Del Gallego'),
(45, 3, 'Gainza'),
(46, 3, 'Garchitorena'),
(47, 3, 'Goa'),
(48, 3, 'Lagonoy'),
(49, 3, 'Libmanan'),
(50, 3, 'Lupi'),
(51, 3, 'Magarao'),
(52, 3, 'Milaor'),
(53, 3, 'Minalabac'),
(54, 3, 'Nabua'),
(55, 3, 'Ocampo'),
(56, 3, 'Pamplona'),
(57, 3, 'Pasacao'),
(58, 3, 'Pili '),
(59, 3, 'Presentacion'),
(60, 3, 'Ragay'),
(61, 3, 'Sagñay'),
(62, 3, 'San Fernando'),
(63, 3, 'San Jose'),
(64, 3, 'Sipocot'),
(65, 3, 'Siruma'),
(66, 3, 'Tigaon'),
(67, 3, 'Tinambac'),
(68, 4, 'Bagamanoc'),
(69, 4, 'Baras'),
(70, 4, 'Bato'),
(71, 4, 'Caramoran'),
(72, 4, 'Gigmoto'),
(73, 4, 'Pandan'),
(74, 4, 'Panganiban (Payo)'),
(75, 4, 'San Andres (Calolbon)'),
(76, 4, 'San Miguel'),
(77, 4, 'Viga'),
(78, 4, 'Virac'),
(79, 5, 'Masbate'),
(80, 5, 'Aroroy'),
(81, 5, 'Baleno'),
(82, 5, 'Balud'),
(83, 5, 'Batuan'),
(84, 5, 'Cataingan'),
(85, 5, 'Cawayan'),
(86, 5, 'Claveria'),
(87, 5, 'Dimasalang'),
(88, 5, 'Esperanza'),
(89, 5, 'Mandaon'),
(90, 5, 'Milagros'),
(91, 5, 'Mobo'),
(92, 5, 'Monreal'),
(93, 5, 'Palanas'),
(94, 5, 'Pio V. Corpuz'),
(95, 5, 'Placer'),
(96, 5, 'San Fernando'),
(97, 5, 'San Jacinto'),
(98, 5, 'San Pascual'),
(99, 5, 'Uson'),
(100, 6, 'Sorsogon'),
(101, 6, 'Barcelona'),
(102, 6, 'Bulan'),
(103, 6, 'Bulusan'),
(104, 6, 'Casiguran'),
(105, 6, 'Castilla'),
(106, 6, 'Donsol'),
(107, 6, 'Gubat'),
(108, 6, 'Irosin'),
(109, 6, 'Juban'),
(110, 6, 'Magallanes'),
(111, 6, 'Matnog'),
(112, 6, 'Pilar'),
(113, 6, 'Prieto Diaz'),
(114, 6, 'Santa Magdalena');

-- --------------------------------------------------------

--
-- Table structure for table `company_logo`
--

CREATE TABLE IF NOT EXISTS `company_logo` (
  `com_id_logo` int(11) NOT NULL,
  `file` varchar(255) NOT NULL,
  `emp_user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_logo`
--

INSERT INTO `company_logo` (`com_id_logo`, `file`, `emp_user_id`) VALUES
(1, 'a0c7889904619b67d38ede93b0a54dea.png', 3),
(3, '553ac95e9e9b76fba25dedaf1bc257c4.png', 2);

-- --------------------------------------------------------

--
-- Table structure for table `company_profile`
--

CREATE TABLE IF NOT EXISTS `company_profile` (
  `com_id` int(11) NOT NULL,
  `flie` varchar(255) NOT NULL,
  `com_description` longtext NOT NULL,
  `emp_user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_profile`
--

INSERT INTO `company_profile` (`com_id`, `flie`, `com_description`, `emp_user_id`) VALUES
(1, '', '<p>From its beginnings as a traditional hardware store in 1976, CitiHardware is now one of the leading and fastest growing construction retail stores with more than 66 branches in the Philippines.</p>\r\n<p>At present, CitiHardware continues to lead in offering its customers great value on world-class products sourced from local and international suppliers. Excellent customer service is a top priority in driving customer loyalty and satisfaction.</p>\r\n<p>CitiHardware is committed to provide its customers &ldquo;Great Value Everyday&rdquo; on products, made from the finest quality materials, at a price you can afford for building projects and home improvement needs.</p>', 3),
(2, '', '<p>SM Supermalls is one of Southeast Asia&rsquo;s biggest developers and the operator of 72 malls in the Philippines, and 8 malls in China. With an average foot traffic of 4.2 million daily in the Philippines, 300,000 in China and over 20,000 tenant partners, SM Supermalls provides family fun experiences as it partners with the best-loved brands and events. SM Supermalls is owned by SM Prime Holdings, Inc., a publicly-listed company and is one of the largest integrated property developers in Southeast Asia.</p>', 2);

-- --------------------------------------------------------

--
-- Table structure for table `dialect`
--

CREATE TABLE IF NOT EXISTS `dialect` (
  `dialect_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `tagalog` int(11) NOT NULL,
  `ilocano` int(11) NOT NULL,
  `ilongo` int(11) NOT NULL,
  `bikol` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `dialect_others` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dialect`
--

INSERT INTO `dialect` (`dialect_id`, `app_user_id`, `tagalog`, `ilocano`, `ilongo`, `bikol`, `others`, `dialect_others`) VALUES
(1, 2, 0, 0, 0, 0, 0, ''),
(2, 5, 0, 0, 0, 0, 0, ''),
(3, 6, 0, 0, 0, 0, 0, ''),
(4, 7, 0, 0, 0, 0, 0, ''),
(5, 13, 0, 0, 0, 0, 0, ''),
(6, 14, 0, 0, 0, 0, 0, ''),
(7, 15, 0, 0, 0, 0, 0, ''),
(8, 17, 0, 0, 0, 0, 0, ''),
(9, 19, 0, 0, 0, 0, 0, ''),
(10, 4, 0, 0, 0, 0, 0, ''),
(11, 5, 0, 0, 0, 0, 0, ''),
(12, 6, 0, 0, 0, 0, 0, ''),
(13, 8, 0, 0, 0, 0, 0, ''),
(14, 7, 1, 0, 0, 1, 0, ''),
(15, 9, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `edu_background`
--

CREATE TABLE IF NOT EXISTS `edu_background` (
  `edu_bg_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `currently_school` varchar(255) NOT NULL,
  `edu_level` varchar(255) NOT NULL,
  `edu_year` varchar(255) NOT NULL,
  `edu_school` varchar(255) NOT NULL,
  `edu_course` varchar(255) NOT NULL,
  `edu_award` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edu_background`
--

INSERT INTO `edu_background` (`edu_bg_id`, `app_user_id`, `currently_school`, `edu_level`, `edu_year`, `edu_school`, `edu_course`, `edu_award`) VALUES
(1, 2, 'No', 'College Graduate', '04-20', 's', 'b', 'n'),
(2, 5, 'No', 'College Graduate', '07-20', 's', 'b', 'n'),
(3, 6, 'No', 'College Graduate', '04-19', 's i ', 'b i ', 'n o'),
(4, 7, 'No', 'College Graduate', '04-20', 's', 'c', 'a'),
(5, 13, 'Yes', 'No Formal Education', '', '', '', ''),
(6, 14, 'Yes', 'No Formal Education', '12-12', 'school', 'course', 'none'),
(7, 15, 'Yes', 'Elementary Graduate', '11-12', 'school', 'course', 'none'),
(8, 17, 'No', 'College Graduate', '04-20', 'STI', 'BSIT', 'None'),
(9, 19, 'No', 'College Graduate', '04-20', 'STI', 'BSIT', 'None'),
(10, 4, 'No', 'College Graduate', '04-20', 'sti', 'bsit', 'none'),
(11, 5, 'Yes', 'No Formal Education', '', '', '', ''),
(12, 6, 'No', 'College Graduate', '', '', '', ''),
(13, 8, 'No', 'College Graduate', '', '', '', ''),
(14, 7, 'Yes', 'Post Graduate', '04-04', 'STI College-Legazpi', 'BS Computer Science', ''),
(15, 9, 'No', 'College Graduate', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `elegibility`
--

CREATE TABLE IF NOT EXISTS `elegibility` (
  `el_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `el_carrer` varchar(255) NOT NULL,
  `el_license` varchar(255) NOT NULL,
  `el_expiry` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `elegibility`
--

INSERT INTO `elegibility` (`el_id`, `app_user_id`, `el_carrer`, `el_license`, `el_expiry`) VALUES
(1, 2, '', '', ''),
(2, 2, '', '', ''),
(3, 5, '', '', ''),
(4, 5, '', '', ''),
(5, 6, '', '', ''),
(6, 6, '', '', ''),
(7, 7, '', '', ''),
(8, 7, '', '', ''),
(9, 13, '', '', ''),
(10, 13, '', '', ''),
(11, 14, '', '', ''),
(12, 14, '', '', ''),
(13, 15, '', '', ''),
(14, 15, '', '', ''),
(15, 17, '', '', ''),
(16, 17, '', '', ''),
(17, 19, '', '', ''),
(18, 19, '', '', ''),
(19, 4, '', '', ''),
(20, 4, '', '', ''),
(21, 5, '', '', ''),
(22, 5, '', '', ''),
(23, 6, '', '', ''),
(24, 6, '', '', ''),
(25, 8, '', '', ''),
(26, 8, '', '', ''),
(27, 7, '', '', ''),
(28, 7, '', '', ''),
(29, 9, '', '', ''),
(30, 9, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `employer_data`
--

CREATE TABLE IF NOT EXISTS `employer_data` (
  `emp_id` int(11) NOT NULL,
  `emp_user_id` int(11) NOT NULL,
  `emp_name` varchar(255) NOT NULL,
  `emp_acronym` varchar(255) NOT NULL,
  `emp_tax` varchar(255) NOT NULL,
  `emp_type` varchar(255) NOT NULL,
  `emp_force` varchar(255) NOT NULL,
  `emp_line_bus` varchar(255) NOT NULL,
  `emp_address` varchar(255) NOT NULL,
  `emp_barangay` varchar(255) NOT NULL,
  `emp_city` varchar(255) NOT NULL,
  `emp_province` varchar(255) NOT NULL,
  `emp_cont_title` varchar(255) NOT NULL,
  `emp_cont_person` varchar(255) NOT NULL,
  `emp_cont_position` varchar(255) NOT NULL,
  `emp_cont_tel` varchar(255) NOT NULL,
  `emp_cont_mobile` varchar(255) NOT NULL,
  `emp_cont_fax` varchar(255) NOT NULL,
  `emp_cont_email` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employer_data`
--

INSERT INTO `employer_data` (`emp_id`, `emp_user_id`, `emp_name`, `emp_acronym`, `emp_tax`, `emp_type`, `emp_force`, `emp_line_bus`, `emp_address`, `emp_barangay`, `emp_city`, `emp_province`, `emp_cont_title`, `emp_cont_person`, `emp_cont_position`, `emp_cont_tel`, `emp_cont_mobile`, `emp_cont_fax`, `emp_cont_email`, `date`) VALUES
(1, 2, 'SM', 'SM', '1', 'Private', 'Large (200 and Up)', '2', 'address', 'barangay', 'city', 'province', 'Mr.', 'O M A', 'HR', '', '+639774243247', '', 'omarpilot11@gmail.com', '2019-11-02'),
(2, 3, 'Citi Hardware', 'Citi', '2', 'Private', 'Large (200 and Up)', '3', 'address', 'barangay', 'city', 'province', 'Mr.', 'A O M', 'Manager', '', '+639186455311', '', 'egyaviator21@gmail.com', '2019-11-02');

-- --------------------------------------------------------

--
-- Table structure for table `job_fair`
--

CREATE TABLE IF NOT EXISTS `job_fair` (
  `jf_id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `jf_title` varchar(255) NOT NULL,
  `slot` int(11) NOT NULL,
  `venue` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `jf_description` longtext NOT NULL,
  `active` int(11) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_fair`
--

INSERT INTO `job_fair` (`jf_id`, `type`, `jf_title`, `slot`, `venue`, `file`, `jf_description`, `active`, `date`) VALUES
(1, '', 'banner', 1, 'lega City', 'peso banner.png', '<p>What is the PESO? The Public Employment Service Office (PESO) is a non-fee charging multi-employment service facility or entity established or accredited pursuant to Republic Act No. 8759, otherwise known as the PESO Act of 1999. The Act provides that in order to carry out full employment and equality of employment opportunities for all, and to strengthen and expand the existing employment facilitation service machinery of the government particularly at the local levels, there shall be established in all capital towns of provinces, key cities, and other strategic areas a Public Employment Service Office. The PESO''s are community-based and maintained largely by local government units (LGU''s) and a number of non-governmental organizations (NGO''s) or community-based organizations (CBO''s) and state universities and colleges (SUC''s). The PESO''s are linked to the regional offices of the Department of Labor and Employment (DOLE) for coordination and technical supervision, and to the DOLE central office, to constitute the national employment service network.</p>', 1, '12-12-19'),
(2, '', 'JOB FAIR', 11, 'lega City', 'a190996f9b520cc869c231069d0c1cbc.jpg', '<p>What is the PESO? The Public Employment Service Office (PESO) is a non-fee charging multi-employment service facility or entity established or accredited pursuant to Republic Act No. 8759, otherwise known as the PESO Act of 1999. The Act provides that in order to carry out full employment and equality of employment opportunities for all, and to strengthen and expand the existing employment facilitation service machinery of the government particularly at the local levels, there shall be established in all capital towns of provinces, key cities, and other strategic areas a Public Employment Service Office. The PESO''s are community-based and maintained largely by local government units (LGU''s) and a number of non-governmental organizations (NGO''s) or community-based organizations (CBO''s) and state universities and colleges (SUC''s). The PESO''s are linked to the regional offices of the Department of Labor and Employment (DOLE) for coordination and technical supervision, and to the DOLE central office, to constitute the national employment service network.</p>', 1, '12-12-19');

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE IF NOT EXISTS `job_post` (
  `job_id` int(11) NOT NULL,
  `emp_user_id` int(11) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `job_description` longtext NOT NULL,
  `job_location` varchar(255) NOT NULL,
  `job_specialization` varchar(255) NOT NULL,
  `job_salary` decimal(10,2) NOT NULL,
  `job_salary_type` varchar(255) NOT NULL,
  `job_shift` varchar(255) NOT NULL,
  `job_type` varchar(255) NOT NULL,
  `job_exp` int(11) NOT NULL,
  `job_slot` int(11) NOT NULL,
  `job_status` int(11) NOT NULL,
  `job_edu_lvl` varchar(255) NOT NULL,
  `denied` int(11) NOT NULL,
  `job_date_create` varchar(255) NOT NULL,
  `expirry_date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`job_id`, `emp_user_id`, `job_title`, `job_description`, `job_location`, `job_specialization`, `job_salary`, `job_salary_type`, `job_shift`, `job_type`, `job_exp`, `job_slot`, `job_status`, `job_edu_lvl`, `denied`, `job_date_create`, `expirry_date`) VALUES
(21, 2, 'Accountant', '<p>Test 2</p>', 'Bicol Region', 'Audit and Taxation', '20000.00', 'Non-Nogtiable', 'Morning', 'Full-Time', 3, 3, 1, 'College Graduate', 0, '2019-11-02', '2020-01-02'),
(23, 3, 'Seller', '<p>Test 4</p>', 'Ilocos Region', 'Advertising', '20000.00', 'Non-Nogtiable', 'Morning', 'Full-Time', 2, 1, 1, 'College Graduate', 0, '2019-11-02', '2020-01-02'),
(24, 2, 'Seller', '<p>test description</p>', 'Legazpi City', 'Advertising', '10000.00', 'Non-Nogtiable', 'Morning', 'Full-Time', 2, 3, 1, 'College Graduate', 0, '2019-11-03', '2020-01-03'),
(25, 2, 'Accountant', '<p>test description&nbsp;</p>', 'Legazpi City', 'Audit and Taxation', '15000.00', 'Non-Nogtiable', 'Morning', 'Full-Time', 1, 5, 1, 'College Graduate', 0, '2019-11-03', '2020-01-03'),
(26, 2, 'Admin', '<p>test description</p>', 'C.A.R', 'IT - Software', '10000.00', 'Non-Nogtiable', 'Morning', 'Full-Time', 2, 4, 1, 'College Graduate', 0, '2019-11-03', '2020-01-03'),
(27, 3, 'Programmer', '<p>test</p>', 'Legazpi City', 'IT - Software', '20000.00', 'Non-Nogtiable', 'Morning', 'Full-Time', 1, 2, 1, 'College Graduate', 0, '2019-11-03', '2020-01-03');

-- --------------------------------------------------------

--
-- Table structure for table `job_preference`
--

CREATE TABLE IF NOT EXISTS `job_preference` (
  `prep_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `occupation1` varchar(255) NOT NULL,
  `occupation2` varchar(255) NOT NULL,
  `occupation3` varchar(255) NOT NULL,
  `industry1` varchar(255) NOT NULL,
  `industry2` varchar(255) NOT NULL,
  `industry3` varchar(255) NOT NULL,
  `salary_expect` decimal(10,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_preference`
--

INSERT INTO `job_preference` (`prep_id`, `app_user_id`, `occupation1`, `occupation2`, `occupation3`, `industry1`, `industry2`, `industry3`, `salary_expect`) VALUES
(1, 2, 'w', '', '', 'i', '', '', '0.00'),
(2, 5, 'oooooooooo', '', '', 'iiiiiiiiiiiii', '', '', '0.00'),
(3, 6, 'oc', '', '', 'in', '', '', '0.00'),
(4, 7, 'o c c', '', '', 'i n d', '', '', '0.00'),
(5, 13, 'as', '', '', 'as', '', '', '123.00'),
(6, 14, 'waiter', '', '', 'resto', '', '', '0.00'),
(7, 15, 'waiter', '', '', 'resturant', '', '', '0.00'),
(8, 17, 'Web Developer', '', '', 'IT', '', '', '0.00'),
(9, 19, 'Web Developer', '', '', 'IT', '', '', '0.00'),
(10, 4, 'Web Developer', '', '', 'IT', '', '', '0.00'),
(11, 5, 'Web Designer', '', '', 'IT', '', '', '0.00'),
(12, 6, 'web designer', '', '', 'IT', '', '', '0.00'),
(13, 8, 'web', '', '', 'it', '', '', '0.00'),
(14, 8, 'web', '', '', 'it', '', '', '0.00'),
(15, 8, 'web', '', '', 'it', '', '', '0.00'),
(16, 8, 'web', '', '', 'it', '', '', '0.00'),
(17, 7, 'Programmer', '', '', 'IT', '', '', '30000.00'),
(18, 9, 'web', '', '', 'it', '', '', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `job_skill`
--

CREATE TABLE IF NOT EXISTS `job_skill` (
  `job_cent_id` int(11) NOT NULL,
  `job_post_id` int(11) NOT NULL,
  `inovation` int(11) NOT NULL,
  `team_work` int(11) NOT NULL,
  `multitasking` int(11) NOT NULL,
  `work_ethics` int(11) NOT NULL,
  `self_motivation` int(11) NOT NULL,
  `creative_problem` int(11) NOT NULL,
  `problem_solving` int(11) NOT NULL,
  `critical_thinking` int(11) NOT NULL,
  `decision_making` int(11) NOT NULL,
  `strees_tolerance` int(11) NOT NULL,
  `planing` int(11) NOT NULL,
  `perceptiveness` int(11) NOT NULL,
  `english_funtional` int(11) NOT NULL,
  `english_comp` int(11) NOT NULL,
  `math_functional` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_skill`
--

INSERT INTO `job_skill` (`job_cent_id`, `job_post_id`, `inovation`, `team_work`, `multitasking`, `work_ethics`, `self_motivation`, `creative_problem`, `problem_solving`, `critical_thinking`, `decision_making`, `strees_tolerance`, `planing`, `perceptiveness`, `english_funtional`, `english_comp`, `math_functional`) VALUES
(15, 14, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(16, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1),
(17, 16, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1),
(18, 17, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(19, 18, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0),
(20, 19, 1, 0, 0, 1, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0),
(21, 20, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0),
(22, 21, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1),
(23, 22, 0, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(24, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1),
(25, 24, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(26, 25, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0),
(27, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1),
(28, 27, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `lang_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `EILTS` int(11) NOT NULL,
  `TOEFL` int(11) NOT NULL,
  `TOCFL` int(11) NOT NULL,
  `JLPT` int(11) NOT NULL,
  `TOPIC` int(11) NOT NULL,
  `lang_other` int(11) NOT NULL,
  `other_specify` varchar(255) NOT NULL,
  `validity_date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`lang_id`, `app_user_id`, `EILTS`, `TOEFL`, `TOCFL`, `JLPT`, `TOPIC`, `lang_other`, `other_specify`, `validity_date`) VALUES
(1, 2, 0, 0, 0, 0, 0, 0, '', ''),
(2, 5, 0, 0, 0, 0, 0, 0, '', ''),
(3, 6, 0, 0, 0, 0, 0, 0, '', ''),
(4, 7, 0, 0, 0, 0, 0, 0, '', ''),
(5, 13, 0, 0, 0, 0, 0, 0, '', ''),
(6, 14, 0, 0, 0, 0, 0, 0, '', ''),
(7, 15, 0, 0, 0, 0, 0, 0, '', ''),
(8, 17, 0, 0, 0, 0, 0, 0, '', ''),
(9, 19, 0, 0, 0, 0, 0, 0, '', ''),
(10, 4, 0, 0, 0, 0, 0, 0, '', ''),
(11, 5, 0, 0, 0, 0, 0, 0, '', ''),
(12, 6, 0, 0, 0, 0, 0, 0, '', ''),
(13, 8, 0, 0, 0, 0, 0, 0, '', ''),
(14, 7, 0, 0, 0, 0, 0, 0, '', ''),
(15, 9, 0, 0, 0, 0, 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE IF NOT EXISTS `province` (
  `province_id` int(11) NOT NULL DEFAULT '0',
  `province_name` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`province_id`, `province_name`) VALUES
(1, 'Albay'),
(2, 'Camarines Norte'),
(3, 'Camarines Sur'),
(4, 'Catanduanes'),
(5, 'Masbate'),
(6, 'Sorsogon');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `res_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `resv_id` varchar(255) NOT NULL,
  `jf_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `resume`
--

CREATE TABLE IF NOT EXISTS `resume` (
  `res_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resume`
--

INSERT INTO `resume` (`res_id`, `user_id`, `file`) VALUES
(1, 19, '343fb49a7cdcbb8fb251af2b2f986635.pdf'),
(2, 6, '73da5df6d666ea88e1461debd62738f5.pdf'),
(3, 7, '32b6496d312c61925893bad4860263f4.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tech_skill`
--

CREATE TABLE IF NOT EXISTS `tech_skill` (
  `tech_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `carpentry` int(11) NOT NULL,
  `masonry` int(11) NOT NULL,
  `welding` int(11) NOT NULL,
  `auto_mechanic` int(11) NOT NULL,
  `plumbing` int(11) NOT NULL,
  `driving` int(11) NOT NULL,
  `gardening` int(11) NOT NULL,
  `tailoring` int(11) NOT NULL,
  `photography` int(11) NOT NULL,
  `hairdressing` int(11) NOT NULL,
  `cook` int(11) NOT NULL,
  `baking` int(11) NOT NULL,
  `other_tech` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tech_skill`
--

INSERT INTO `tech_skill` (`tech_id`, `app_user_id`, `carpentry`, `masonry`, `welding`, `auto_mechanic`, `plumbing`, `driving`, `gardening`, `tailoring`, `photography`, `hairdressing`, `cook`, `baking`, `other_tech`) VALUES
(1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(2, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(3, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(4, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(5, 13, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(6, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(7, 15, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(8, 17, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, ''),
(9, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(10, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(11, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(12, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(13, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(14, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(15, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tec_training`
--

CREATE TABLE IF NOT EXISTS `tec_training` (
  `tec_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `tec_cur_training` varchar(255) NOT NULL,
  `tec_training` varchar(255) NOT NULL,
  `tec_duration` varchar(255) NOT NULL,
  `tec_insti` varchar(255) NOT NULL,
  `tec_cert` varchar(255) NOT NULL,
  `tec_complete` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tec_training`
--

INSERT INTO `tec_training` (`tec_id`, `app_user_id`, `tec_cur_training`, `tec_training`, `tec_duration`, `tec_insti`, `tec_cert`, `tec_complete`) VALUES
(1, 2, '', '', '', '', '', 'Yes'),
(2, 2, '', '', '', '', '', 'Yes'),
(3, 2, '', '', '', '', '', 'Yes'),
(4, 5, '', '', '', '', '', 'Yes'),
(5, 5, '', '', '', '', '', 'Yes'),
(6, 5, '', '', '', '', '', 'Yes'),
(7, 6, '', '', '', '', '', 'Yes'),
(8, 6, '', '', '', '', '', 'Yes'),
(9, 6, '', '', '', '', '', 'Yes'),
(10, 7, '', '', '', '', '', 'Yes'),
(11, 7, '', '', '', '', '', 'Yes'),
(12, 7, '', '', '', '', '', 'Yes'),
(13, 13, '', '', '', '', '', 'Yes'),
(14, 13, '', '', '', '', '', 'Yes'),
(15, 13, '', '', '', '', '', 'Yes'),
(16, 14, '', '', '', '', '', 'Yes'),
(17, 14, '', '', '', '', '', 'Yes'),
(18, 14, '', '', '', '', '', 'Yes'),
(19, 15, '', '', '', '', '', 'Yes'),
(20, 15, '', '', '', '', '', 'Yes'),
(21, 15, '', '', '', '', '', 'Yes'),
(22, 17, '', '', '', '', '', 'Yes'),
(23, 17, '', '', '', '', '', 'Yes'),
(24, 17, '', '', '', '', '', 'Yes'),
(25, 19, '', '', '', '', '', 'Yes'),
(26, 19, '', '', '', '', '', 'Yes'),
(27, 19, '', '', '', '', '', 'Yes'),
(28, 4, '', '', '', '', '', 'Yes'),
(29, 4, '', '', '', '', '', 'Yes'),
(30, 4, '', '', '', '', '', 'Yes'),
(31, 5, '', '', '', '', '', 'Yes'),
(32, 5, '', '', '', '', '', 'Yes'),
(33, 5, '', '', '', '', '', 'Yes'),
(34, 6, '', '', '', '', '', 'Yes'),
(35, 6, '', '', '', '', '', 'Yes'),
(36, 6, '', '', '', '', '', 'Yes'),
(37, 8, '', '', '', '', '', 'Yes'),
(38, 8, '', '', '', '', '', 'Yes'),
(39, 8, '', '', '', '', '', 'Yes'),
(40, 7, '', '', '', '', '', 'Yes'),
(41, 7, '', '', '', '', '', 'Yes'),
(42, 7, '', '', '', '', '', 'Yes'),
(43, 9, '', '', '', '', '', 'Yes'),
(44, 9, '', '', '', '', '', 'Yes'),
(45, 9, '', '', '', '', '', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE IF NOT EXISTS `user_account` (
  `user_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activate` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `denied` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `last_login` varchar(255) NOT NULL,
  `confirm` int(11) NOT NULL,
  `confirm_code` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_account`
--

INSERT INTO `user_account` (`user_id`, `email`, `password`, `activate`, `role`, `denied`, `token`, `last_login`, `confirm`, `confirm_code`, `date`) VALUES
(1, 'admin@mail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1, 0, '', '11-04-19', 1, '', ''),
(2, 'omarpilot11@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 3, 0, '', '11-04-19', 1, 'C1cXS', '2019-11-02'),
(3, 'egyaviator21@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 3, 0, '', '11-04-19', 1, 'CIVqA', '2019-11-02'),
(4, 'seliemomar1997.ph@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 4, 0, '', '', 1, 'fYqLd', '2019-11-03'),
(6, 'seliemomar1997.eg@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 4, 0, '', '11-03-19', 1, '9gRbY', '2019-11-03'),
(7, 'harly.23@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 4, 0, '', '11-03-19', 1, 'qlZCf', '2019-11-03'),
(8, 'omarseliem1997.ph@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, 4, 0, '', '11-04-19', 1, 'i0QVU', '2019-11-03'),
(9, 'omarseliem1997.eg@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, 4, 0, '', '', 1, 'dRYvp', '2019-11-03');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list`
--

CREATE TABLE IF NOT EXISTS `wish_list` (
  `wish_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `work_exp`
--

CREATE TABLE IF NOT EXISTS `work_exp` (
  `work_exp_id` int(11) NOT NULL,
  `app_user_id` int(11) NOT NULL,
  `work_company` varchar(255) NOT NULL,
  `work_address` varchar(255) NOT NULL,
  `work_position` varchar(255) NOT NULL,
  `work_date` varchar(255) NOT NULL,
  `work_status` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `work_exp`
--

INSERT INTO `work_exp` (`work_exp_id`, `app_user_id`, `work_company`, `work_address`, `work_position`, `work_date`, `work_status`) VALUES
(1, 2, '', '', '', '', ''),
(2, 2, '', '', '', '', ''),
(3, 2, '', '', '', '', ''),
(4, 5, '', '', '', '', ''),
(5, 5, '', '', '', '', ''),
(6, 5, '', '', '', '', ''),
(7, 6, '', '', '', '', ''),
(8, 6, '', '', '', '', ''),
(9, 6, '', '', '', '', ''),
(10, 7, '', '', '', '', ''),
(11, 7, '', '', '', '', ''),
(12, 7, '', '', '', '', ''),
(13, 13, '', '', '', '', ''),
(14, 13, '', '', '', '', ''),
(15, 13, '', '', '', '', ''),
(16, 14, '', '', '', '', ''),
(17, 14, '', '', '', '', ''),
(18, 14, '', '', '', '', ''),
(19, 15, '', '', '', '', ''),
(20, 15, '', '', '', '', ''),
(21, 15, '', '', '', '', ''),
(22, 17, '', '', '', '', ''),
(23, 17, '', '', '', '', ''),
(24, 17, '', '', '', '', ''),
(25, 19, '', '', '', '', ''),
(26, 19, '', '', '', '', ''),
(27, 19, '', '', '', '', ''),
(28, 4, '', '', '', '', ''),
(29, 4, '', '', '', '', ''),
(30, 4, '', '', '', '', ''),
(31, 5, '', '', '', '', ''),
(32, 5, '', '', '', '', ''),
(33, 5, '', '', '', '', ''),
(34, 6, '', '', '', '', ''),
(35, 6, '', '', '', '', ''),
(36, 6, '', '', '', '', ''),
(37, 8, '', '', '', '', ''),
(38, 8, '', '', '', '', ''),
(39, 8, '', '', '', '', ''),
(40, 7, '', '', '', '', ''),
(41, 7, '', '', '', '', ''),
(42, 7, '', '', '', '', ''),
(43, 9, '', '', '', '', ''),
(44, 9, '', '', '', '', ''),
(45, 9, '', '', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicant_personal`
--
ALTER TABLE `applicant_personal`
  ADD PRIMARY KEY (`personal_id`);

--
-- Indexes for table `applied_job`
--
ALTER TABLE `applied_job`
  ADD PRIMARY KEY (`app_id`);

--
-- Indexes for table `app_picture`
--
ALTER TABLE `app_picture`
  ADD PRIMARY KEY (`pic_id`);

--
-- Indexes for table `barangays`
--
ALTER TABLE `barangays`
  ADD PRIMARY KEY (`barangay_id`);

--
-- Indexes for table `century_skill`
--
ALTER TABLE `century_skill`
  ADD PRIMARY KEY (`cent_id`);

--
-- Indexes for table `city_municipality`
--
ALTER TABLE `city_municipality`
  ADD PRIMARY KEY (`city_municipality_id`);

--
-- Indexes for table `company_logo`
--
ALTER TABLE `company_logo`
  ADD PRIMARY KEY (`com_id_logo`);

--
-- Indexes for table `company_profile`
--
ALTER TABLE `company_profile`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `dialect`
--
ALTER TABLE `dialect`
  ADD PRIMARY KEY (`dialect_id`);

--
-- Indexes for table `edu_background`
--
ALTER TABLE `edu_background`
  ADD PRIMARY KEY (`edu_bg_id`);

--
-- Indexes for table `elegibility`
--
ALTER TABLE `elegibility`
  ADD PRIMARY KEY (`el_id`);

--
-- Indexes for table `employer_data`
--
ALTER TABLE `employer_data`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `job_fair`
--
ALTER TABLE `job_fair`
  ADD PRIMARY KEY (`jf_id`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
  ADD PRIMARY KEY (`job_id`);

--
-- Indexes for table `job_preference`
--
ALTER TABLE `job_preference`
  ADD PRIMARY KEY (`prep_id`);

--
-- Indexes for table `job_skill`
--
ALTER TABLE `job_skill`
  ADD PRIMARY KEY (`job_cent_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`province_id`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `resume`
--
ALTER TABLE `resume`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `tech_skill`
--
ALTER TABLE `tech_skill`
  ADD PRIMARY KEY (`tech_id`);

--
-- Indexes for table `tec_training`
--
ALTER TABLE `tec_training`
  ADD PRIMARY KEY (`tec_id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `wish_list`
--
ALTER TABLE `wish_list`
  ADD PRIMARY KEY (`wish_id`);

--
-- Indexes for table `work_exp`
--
ALTER TABLE `work_exp`
  ADD PRIMARY KEY (`work_exp_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicant_personal`
--
ALTER TABLE `applicant_personal`
  MODIFY `personal_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `applied_job`
--
ALTER TABLE `applied_job`
  MODIFY `app_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `app_picture`
--
ALTER TABLE `app_picture`
  MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `barangays`
--
ALTER TABLE `barangays`
  MODIFY `barangay_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=447;
--
-- AUTO_INCREMENT for table `century_skill`
--
ALTER TABLE `century_skill`
  MODIFY `cent_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `company_logo`
--
ALTER TABLE `company_logo`
  MODIFY `com_id_logo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `company_profile`
--
ALTER TABLE `company_profile`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `dialect`
--
ALTER TABLE `dialect`
  MODIFY `dialect_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `edu_background`
--
ALTER TABLE `edu_background`
  MODIFY `edu_bg_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `elegibility`
--
ALTER TABLE `elegibility`
  MODIFY `el_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `employer_data`
--
ALTER TABLE `employer_data`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `job_fair`
--
ALTER TABLE `job_fair`
  MODIFY `jf_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
  MODIFY `job_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `job_preference`
--
ALTER TABLE `job_preference`
  MODIFY `prep_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `job_skill`
--
ALTER TABLE `job_skill`
  MODIFY `job_cent_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resume`
--
ALTER TABLE `resume`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tech_skill`
--
ALTER TABLE `tech_skill`
  MODIFY `tech_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tec_training`
--
ALTER TABLE `tec_training`
  MODIFY `tec_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `wish_list`
--
ALTER TABLE `wish_list`
  MODIFY `wish_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `work_exp`
--
ALTER TABLE `work_exp`
  MODIFY `work_exp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
